package com.hospitals.dh.web.rest;

import com.hospitals.dh.HospitalsApp;
import com.hospitals.dh.domain.VisitNote;
import com.hospitals.dh.repository.VisitNoteRepository;
import com.hospitals.dh.service.VisitNoteService;
import com.hospitals.dh.service.dto.VisitNoteDTO;
import com.hospitals.dh.service.mapper.VisitNoteMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link VisitNoteResource} REST controller.
 */
@SpringBootTest(classes = HospitalsApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class VisitNoteResourceIT {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_VISIT_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_VISIT_DATE = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private VisitNoteRepository visitNoteRepository;

    @Autowired
    private VisitNoteMapper visitNoteMapper;

    @Autowired
    private VisitNoteService visitNoteService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restVisitNoteMockMvc;

    private VisitNote visitNote;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static VisitNote createEntity(EntityManager em) {
        VisitNote visitNote = new VisitNote()
            .description(DEFAULT_DESCRIPTION)
            .visitDate(DEFAULT_VISIT_DATE);
        return visitNote;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static VisitNote createUpdatedEntity(EntityManager em) {
        VisitNote visitNote = new VisitNote()
            .description(UPDATED_DESCRIPTION)
            .visitDate(UPDATED_VISIT_DATE);
        return visitNote;
    }

    @BeforeEach
    public void initTest() {
        visitNote = createEntity(em);
    }

    @Test
    @Transactional
    public void createVisitNote() throws Exception {
        int databaseSizeBeforeCreate = visitNoteRepository.findAll().size();
        // Create the VisitNote
        VisitNoteDTO visitNoteDTO = visitNoteMapper.toDto(visitNote);
        restVisitNoteMockMvc.perform(post("/api/visit-notes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(visitNoteDTO)))
            .andExpect(status().isCreated());

        // Validate the VisitNote in the database
        List<VisitNote> visitNoteList = visitNoteRepository.findAll();
        assertThat(visitNoteList).hasSize(databaseSizeBeforeCreate + 1);
        VisitNote testVisitNote = visitNoteList.get(visitNoteList.size() - 1);
        assertThat(testVisitNote.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testVisitNote.getVisitDate()).isEqualTo(DEFAULT_VISIT_DATE);
    }

    @Test
    @Transactional
    public void createVisitNoteWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = visitNoteRepository.findAll().size();

        // Create the VisitNote with an existing ID
        visitNote.setId(1L);
        VisitNoteDTO visitNoteDTO = visitNoteMapper.toDto(visitNote);

        // An entity with an existing ID cannot be created, so this API call must fail
        restVisitNoteMockMvc.perform(post("/api/visit-notes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(visitNoteDTO)))
            .andExpect(status().isBadRequest());

        // Validate the VisitNote in the database
        List<VisitNote> visitNoteList = visitNoteRepository.findAll();
        assertThat(visitNoteList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkVisitDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = visitNoteRepository.findAll().size();
        // set the field null
        visitNote.setVisitDate(null);

        // Create the VisitNote, which fails.
        VisitNoteDTO visitNoteDTO = visitNoteMapper.toDto(visitNote);


        restVisitNoteMockMvc.perform(post("/api/visit-notes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(visitNoteDTO)))
            .andExpect(status().isBadRequest());

        List<VisitNote> visitNoteList = visitNoteRepository.findAll();
        assertThat(visitNoteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllVisitNotes() throws Exception {
        // Initialize the database
        visitNoteRepository.saveAndFlush(visitNote);

        // Get all the visitNoteList
        restVisitNoteMockMvc.perform(get("/api/visit-notes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(visitNote.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].visitDate").value(hasItem(DEFAULT_VISIT_DATE.toString())));
    }
    
    @Test
    @Transactional
    public void getVisitNote() throws Exception {
        // Initialize the database
        visitNoteRepository.saveAndFlush(visitNote);

        // Get the visitNote
        restVisitNoteMockMvc.perform(get("/api/visit-notes/{id}", visitNote.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(visitNote.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.visitDate").value(DEFAULT_VISIT_DATE.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingVisitNote() throws Exception {
        // Get the visitNote
        restVisitNoteMockMvc.perform(get("/api/visit-notes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateVisitNote() throws Exception {
        // Initialize the database
        visitNoteRepository.saveAndFlush(visitNote);

        int databaseSizeBeforeUpdate = visitNoteRepository.findAll().size();

        // Update the visitNote
        VisitNote updatedVisitNote = visitNoteRepository.findById(visitNote.getId()).get();
        // Disconnect from session so that the updates on updatedVisitNote are not directly saved in db
        em.detach(updatedVisitNote);
        updatedVisitNote
            .description(UPDATED_DESCRIPTION)
            .visitDate(UPDATED_VISIT_DATE);
        VisitNoteDTO visitNoteDTO = visitNoteMapper.toDto(updatedVisitNote);

        restVisitNoteMockMvc.perform(put("/api/visit-notes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(visitNoteDTO)))
            .andExpect(status().isOk());

        // Validate the VisitNote in the database
        List<VisitNote> visitNoteList = visitNoteRepository.findAll();
        assertThat(visitNoteList).hasSize(databaseSizeBeforeUpdate);
        VisitNote testVisitNote = visitNoteList.get(visitNoteList.size() - 1);
        assertThat(testVisitNote.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testVisitNote.getVisitDate()).isEqualTo(UPDATED_VISIT_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingVisitNote() throws Exception {
        int databaseSizeBeforeUpdate = visitNoteRepository.findAll().size();

        // Create the VisitNote
        VisitNoteDTO visitNoteDTO = visitNoteMapper.toDto(visitNote);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restVisitNoteMockMvc.perform(put("/api/visit-notes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(visitNoteDTO)))
            .andExpect(status().isBadRequest());

        // Validate the VisitNote in the database
        List<VisitNote> visitNoteList = visitNoteRepository.findAll();
        assertThat(visitNoteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteVisitNote() throws Exception {
        // Initialize the database
        visitNoteRepository.saveAndFlush(visitNote);

        int databaseSizeBeforeDelete = visitNoteRepository.findAll().size();

        // Delete the visitNote
        restVisitNoteMockMvc.perform(delete("/api/visit-notes/{id}", visitNote.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<VisitNote> visitNoteList = visitNoteRepository.findAll();
        assertThat(visitNoteList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
