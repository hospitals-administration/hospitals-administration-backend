package com.hospitals.dh.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class SpecialityMapperTest {

    private SpecialityMapper specialityMapper;

    @BeforeEach
    public void setUp() {
        specialityMapper = new SpecialityMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(specialityMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(specialityMapper.fromId(null)).isNull();
    }
}
