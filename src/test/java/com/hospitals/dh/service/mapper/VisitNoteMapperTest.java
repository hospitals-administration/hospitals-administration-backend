package com.hospitals.dh.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class VisitNoteMapperTest {

    private VisitNoteMapper visitNoteMapper;

    @BeforeEach
    public void setUp() {
        visitNoteMapper = new VisitNoteMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(visitNoteMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(visitNoteMapper.fromId(null)).isNull();
    }
}
