package com.hospitals.dh.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hospitals.dh.web.rest.TestUtil;

public class VisitNoteDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(VisitNoteDTO.class);
        VisitNoteDTO visitNoteDTO1 = new VisitNoteDTO();
        visitNoteDTO1.setId(1L);
        VisitNoteDTO visitNoteDTO2 = new VisitNoteDTO();
        assertThat(visitNoteDTO1).isNotEqualTo(visitNoteDTO2);
        visitNoteDTO2.setId(visitNoteDTO1.getId());
        assertThat(visitNoteDTO1).isEqualTo(visitNoteDTO2);
        visitNoteDTO2.setId(2L);
        assertThat(visitNoteDTO1).isNotEqualTo(visitNoteDTO2);
        visitNoteDTO1.setId(null);
        assertThat(visitNoteDTO1).isNotEqualTo(visitNoteDTO2);
    }
}
