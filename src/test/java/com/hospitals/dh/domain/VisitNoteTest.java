package com.hospitals.dh.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.hospitals.dh.web.rest.TestUtil;

public class VisitNoteTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(VisitNote.class);
        VisitNote visitNote1 = new VisitNote();
        visitNote1.setId(1L);
        VisitNote visitNote2 = new VisitNote();
        visitNote2.setId(visitNote1.getId());
        assertThat(visitNote1).isEqualTo(visitNote2);
        visitNote2.setId(2L);
        assertThat(visitNote1).isNotEqualTo(visitNote2);
        visitNote1.setId(null);
        assertThat(visitNote1).isNotEqualTo(visitNote2);
    }
}
