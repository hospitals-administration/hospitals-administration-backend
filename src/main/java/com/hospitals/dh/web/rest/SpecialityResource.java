package com.hospitals.dh.web.rest;

import com.hospitals.dh.domain.Speciality;
import com.hospitals.dh.repository.SpecialityRepository;
import com.hospitals.dh.service.SpecialityService;
import com.hospitals.dh.web.rest.errors.BadRequestAlertException;
import com.hospitals.dh.service.dto.SpecialityDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.hospitals.dh.domain.Speciality}.
 */
@RestController
@RequestMapping("/api")
public class SpecialityResource {

    private final Logger log = LoggerFactory.getLogger(SpecialityResource.class);

    private static final String ENTITY_NAME = "speciality";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SpecialityService specialityService;
    private final SpecialityRepository specialityRepository;

    public SpecialityResource(SpecialityService specialityService, SpecialityRepository specialityRepository) {
        this.specialityService = specialityService;
        this.specialityRepository = specialityRepository;
    }

    /**
     * {@code POST  /specialities} : Create a new speciality.
     *
     * @param specialityDTO the specialityDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new specialityDTO, or with status {@code 400 (Bad Request)} if the speciality has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/specialities")
    public ResponseEntity<SpecialityDTO> createSpeciality(@Valid @RequestBody SpecialityDTO specialityDTO) throws URISyntaxException {
        log.debug("REST request to save Speciality : {}", specialityDTO);
        if (specialityDTO.getId() != null) {
            throw new BadRequestAlertException("A new speciality cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Optional<Speciality> speciality = this.specialityRepository.findOneByName(specialityDTO.getName());
        if (speciality.isPresent()) {
            throw new BadRequestAlertException("The speciality name already exists", ENTITY_NAME, "idexists");
        }
        SpecialityDTO result = specialityService.save(specialityDTO);
        return ResponseEntity.created(new URI("/api/specialities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /specialities} : Updates an existing speciality.
     *
     * @param specialityDTO the specialityDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated specialityDTO,
     * or with status {@code 400 (Bad Request)} if the specialityDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the specialityDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/specialities")
    public ResponseEntity<SpecialityDTO> updateSpeciality(@Valid @RequestBody SpecialityDTO specialityDTO) throws URISyntaxException {
        log.debug("REST request to update Speciality : {}", specialityDTO);
        if (specialityDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Optional<Speciality> speciality = this.specialityRepository.findOneByName(specialityDTO.getName());
        if (speciality.isPresent()) {
            if (speciality.get().getId() != specialityDTO.getId()) {
                throw new BadRequestAlertException("The speciality name already exists", ENTITY_NAME, "idexists");
            }
        }
        SpecialityDTO result = specialityService.save(specialityDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, specialityDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /specialities} : get all the specialities.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of specialities in body.
     */
    @GetMapping("/specialities")
    public ResponseEntity<List<SpecialityDTO>> getAllSpecialities(Pageable pageable) {
        log.debug("REST request to get a page of Specialities");
        Page<SpecialityDTO> page = specialityService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /specialities/:id} : get the "id" speciality.
     *
     * @param id the id of the specialityDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the specialityDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/specialities/{id}")
    public ResponseEntity<SpecialityDTO> getSpeciality(@PathVariable Long id) {
        log.debug("REST request to get Speciality : {}", id);
        Optional<SpecialityDTO> specialityDTO = specialityService.findOne(id);
        return ResponseUtil.wrapOrNotFound(specialityDTO);
    }

    /**
     * {@code DELETE  /specialities/:id} : delete the "id" speciality.
     *
     * @param id the id of the specialityDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/specialities/{id}")
    public ResponseEntity<Void> deleteSpeciality(@PathVariable Long id) {
        log.debug("REST request to delete Speciality : {}", id);
        specialityService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    @PostMapping("/specialities/update-current-avatar")
    public ResponseEntity<SpecialityDTO> updateCurrentAvatarUser(@RequestParam("userId") Long id,
            @RequestParam("file") MultipartFile file) throws IllegalStateException, IOException {
        SpecialityDTO response = specialityService.addAvatar(id, file);
        return ResponseEntity.ok().body(response);
    }

}
