/**
 * View Models used by Spring MVC REST controllers.
 */
package com.hospitals.dh.web.rest.vm;
