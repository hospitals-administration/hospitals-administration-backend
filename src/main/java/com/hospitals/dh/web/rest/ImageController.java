package com.hospitals.dh.web.rest;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.compress.utils.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/image")
public class ImageController {
    
    private final Logger log = LoggerFactory.getLogger(ImageController.class);

    private static final String ENTITY_NAME = "IMAGE";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    public ImageController () {
    }

    @GetMapping("/{entity}/get-image/{filename:.+}")
    public void getImageFile(@PathVariable String filename, HttpServletResponse response, 
        HttpServletRequest request, String url) throws IOException {
        String home = System.getProperty("user.home");
        String dirLocation = home + "/.images" + url;
        System.out.println(dirLocation);
        ServletContext cntx = request.getServletContext();
        String mime = cntx.getMimeType(filename);
        if (mime == null) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
        response.setContentType(mime);
        String dir = dirLocation;
        File file = new File(dir + "/" + filename); 
        response.setContentLength((int)file.length());
        FileInputStream in = new FileInputStream(file);
        System.out.println(mime);
        IOUtils.copy(in, response.getOutputStream());
    }

}

