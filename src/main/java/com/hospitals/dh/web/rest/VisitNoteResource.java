package com.hospitals.dh.web.rest;

import com.hospitals.dh.service.VisitNoteService;
import com.hospitals.dh.web.rest.errors.BadRequestAlertException;
import com.hospitals.dh.service.dto.VisitNoteDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.hospitals.dh.domain.VisitNote}.
 */
@RestController
@RequestMapping("/api")
public class VisitNoteResource {

    private final Logger log = LoggerFactory.getLogger(VisitNoteResource.class);

    private static final String ENTITY_NAME = "visitNote";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final VisitNoteService visitNoteService;

    public VisitNoteResource(VisitNoteService visitNoteService) {
        this.visitNoteService = visitNoteService;
    }

    /**
     * {@code POST  /visit-notes} : Create a new visitNote.
     *
     * @param visitNoteDTO the visitNoteDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new visitNoteDTO, or with status {@code 400 (Bad Request)} if the visitNote has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/visit-notes")
    public ResponseEntity<VisitNoteDTO> createVisitNote(@Valid @RequestBody VisitNoteDTO visitNoteDTO) throws URISyntaxException {
        log.debug("REST request to save VisitNote : {}", visitNoteDTO);
        if (visitNoteDTO.getId() != null) {
            throw new BadRequestAlertException("A new visitNote cannot already have an ID", ENTITY_NAME, "idexists");
        }
        VisitNoteDTO result = visitNoteService.save(visitNoteDTO);
        return ResponseEntity.created(new URI("/api/visit-notes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /visit-notes} : Updates an existing visitNote.
     *
     * @param visitNoteDTO the visitNoteDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated visitNoteDTO,
     * or with status {@code 400 (Bad Request)} if the visitNoteDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the visitNoteDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/visit-notes")
    public ResponseEntity<VisitNoteDTO> updateVisitNote(@Valid @RequestBody VisitNoteDTO visitNoteDTO) throws URISyntaxException {
        log.debug("REST request to update VisitNote : {}", visitNoteDTO);
        if (visitNoteDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        VisitNoteDTO result = visitNoteService.save(visitNoteDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, visitNoteDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /visit-notes} : get all the visitNotes.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of visitNotes in body.
     */
    @GetMapping("/visit-notes")
    public ResponseEntity<List<VisitNoteDTO>> getAllVisitNotes(Pageable pageable) {
        log.debug("REST request to get a page of VisitNotes");
        Page<VisitNoteDTO> page = visitNoteService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /visit-notes/:id} : get the "id" visitNote.
     *
     * @param id the id of the visitNoteDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the visitNoteDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/visit-notes/{id}")
    public ResponseEntity<VisitNoteDTO> getVisitNote(@PathVariable Long id) {
        log.debug("REST request to get VisitNote : {}", id);
        Optional<VisitNoteDTO> visitNoteDTO = visitNoteService.findOne(id);
        return ResponseUtil.wrapOrNotFound(visitNoteDTO);
    }

    /**
     * {@code DELETE  /visit-notes/:id} : delete the "id" visitNote.
     *
     * @param id the id of the visitNoteDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/visit-notes/{id}")
    public ResponseEntity<Void> deleteVisitNote(@PathVariable Long id) {
        log.debug("REST request to delete VisitNote : {}", id);
        visitNoteService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
    
    // Visit notes by patient

    @GetMapping("/visit-notes/patient/{id}")
    public ResponseEntity<List<VisitNoteDTO>> getAllVisitNotesByPatientId(@PathVariable Long id, Pageable pageable) {
        log.debug("REST request to get a page of VisitNotes");
        Page<VisitNoteDTO> page = visitNoteService.findAllVisitNoteByPatientId(id, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

}
