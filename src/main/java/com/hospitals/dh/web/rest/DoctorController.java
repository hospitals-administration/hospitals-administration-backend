package com.hospitals.dh.web.rest;

import com.hospitals.dh.repository.UserRepository;
import com.hospitals.dh.service.DoctorService;
import com.hospitals.dh.service.MailService;
import com.hospitals.dh.service.UserService;

import com.hospitals.dh.config.Constants;
import com.hospitals.dh.domain.User;
import com.hospitals.dh.repository.UserRepository;
import com.hospitals.dh.security.AuthoritiesConstants;
import com.hospitals.dh.service.MailService;
import org.springframework.data.domain.Sort;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Collections;
import com.hospitals.dh.service.UserService;
import com.hospitals.dh.service.dto.UserDTO;
import com.hospitals.dh.web.rest.errors.BadRequestAlertException;
import com.hospitals.dh.web.rest.errors.EmailAlreadyUsedException;
import com.hospitals.dh.web.rest.errors.LoginAlreadyUsedException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.List;


@RestController
@RequestMapping("/api")
public class DoctorController {
    // private static final List<String> ALLOWED_ORDERED_PROPERTIES = Collections.unmodifiableList(Arrays.asList("id", "login", "firstName", "lastName", "email", "activated", "langKey"));

    private final Logger log = LoggerFactory.getLogger(DoctorController.class);

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UserService userService;

    private final DoctorService doctorService;

    private final UserRepository userRepository;

    private final MailService mailService;

    public DoctorController(
        UserService userService, 
        UserRepository userRepository, 
        MailService mailService,
        DoctorService doctorService
    ) {
        this.userService = userService;
        this.userRepository = userRepository;
        this.mailService = mailService;
        this.doctorService = doctorService;
    }

    @GetMapping("/doctors")
    public ResponseEntity<List<UserDTO>> getAllDoctors(Pageable pageable) {
        Page<UserDTO> page = doctorService.findAllUsersByDoctorRole(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/doctors/hospital/{id}")
    public ResponseEntity<List<UserDTO>> getAllDoctorsByHospitalId(@PathVariable Long id, Pageable pageable) {
        Page<UserDTO> page = doctorService.findAllUsersByDoctorRoleAndHospitalId(id.toString(), pageable);
        // Page<UserDTO> page = doctorService.findAllUsersByDoctorRole(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @PostMapping("/doctors")
    public ResponseEntity<User> createUser(@Valid @RequestBody UserDTO userDTO) throws URISyntaxException {
        log.debug("REST request to save Doctor : {}", userDTO);
        String email = userDTO.getLogin() + "@localhost.com";
        if (userDTO.getId() != null) {
            throw new BadRequestAlertException("A new user cannot already have an ID", "userManagement", "idexists");
            // Lowercase the user login before comparing with database
        } else if (userRepository.findOneByLogin(userDTO.getLogin().toLowerCase()).isPresent()) {
            throw new LoginAlreadyUsedException();
        } else if (userRepository.findOneByEmailIgnoreCase(email).isPresent()) {
            throw new EmailAlreadyUsedException();
        } else {
            User newUser = this.doctorService.CreateDoctor(userDTO, userDTO.getLogin());
            // mailService.sendCreationEmail(newUser);
            return ResponseEntity.created(new URI("/api/doctors" + newUser.getLogin()))
                .headers(HeaderUtil.createAlert(applicationName,  "userManagement.created", newUser.getLogin()))
                .body(newUser);
        }
    }

    @PutMapping("/doctors")
    public ResponseEntity<UserDTO> updateUser(@Valid @RequestBody UserDTO userDTO) throws URISyntaxException {
        log.debug("REST request to update Doctor : {}", userDTO);
        Optional<UserDTO> updatedUser = doctorService.updateDoctor(userDTO);
        return ResponseUtil.wrapOrNotFound(updatedUser,
            HeaderUtil.createAlert(applicationName, "userManagement.updated", userDTO.getLogin()));
    }

    @DeleteMapping("/doctors/{id}")
    public ResponseEntity<Void> deleteUser(@PathVariable Long id) {
        log.debug("REST request to delete User: {}", id);
        doctorService.deleteDoctor(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createAlert(applicationName,  "userManagement.deleted", id.toString())).build();
    }

    @PostMapping("/doctors/update-current-avatar")
    public ResponseEntity<UserDTO> updateCurrentAvatarUser(@RequestParam("userId") Long id,
            @RequestParam("file") MultipartFile file) throws IllegalStateException, IOException {
        UserDTO response = doctorService.addAvatar(id, file);
        return ResponseEntity.ok().body(response);
    }

    // =============================================== SEARCH =================================================

    @GetMapping("/doctors/search")
    public ResponseEntity<List<UserDTO>> searchByNameOrCreatedDate(
        @RequestParam(required = false) String name, @RequestParam(required = false) 
        @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate createdate, Pageable pageable) throws IOException {
        log.debug("REST request to search Hospitals");
        String date = Objects.isNull(createdate) ?  null : createdate.toString();
        String na = Objects.isNull(name) ?  null : name.toString();
        Page<UserDTO> page = doctorService.searchDoctorsByNameOrCreatedDate(na, date, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

}