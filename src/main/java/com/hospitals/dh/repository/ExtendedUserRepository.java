package com.hospitals.dh.repository;

import com.hospitals.dh.domain.ExtendedUser;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the ExtendedUser entity.
 */
@Repository
public interface ExtendedUserRepository extends JpaRepository<ExtendedUser, Long> {

    @Query(value = "select distinct extendedUser from ExtendedUser extendedUser left join fetch extendedUser.specialities",
        countQuery = "select count(distinct extendedUser) from ExtendedUser extendedUser")
    Page<ExtendedUser> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct extendedUser from ExtendedUser extendedUser left join fetch extendedUser.specialities")
    List<ExtendedUser> findAllWithEagerRelationships();

    @Query("select extendedUser from ExtendedUser extendedUser left join fetch extendedUser.specialities where extendedUser.id =:id")
    Optional<ExtendedUser> findOneWithEagerRelationships(@Param("id") Long id);
}
