package com.hospitals.dh.repository;

import com.hospitals.dh.domain.VisitNote;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the VisitNote entity.
 */
@SuppressWarnings("unused")
@Repository
public interface VisitNoteRepository extends JpaRepository<VisitNote, Long> {

    @Query("select visitNote from VisitNote visitNote where visitNote.userPatient.login = ?#{principal.username}")
    List<VisitNote> findByUserPatientIsCurrentUser();

    @Query("select visitNote from VisitNote visitNote where visitNote.userDoctor.login = ?#{principal.username}")
    List<VisitNote> findByUserDoctorIsCurrentUser();

    @Query("select visitNote from VisitNote visitNote where visitNote.userPatient.id = :patientId")
    Page<VisitNote> findAllByUserPatientId(@Param("patientId") Long patientId, Pageable pageable);

}
