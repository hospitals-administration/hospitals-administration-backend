package com.hospitals.dh.repository;

import java.time.Instant;
import java.util.Optional;

import com.hospitals.dh.domain.Hospital;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Hospital entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HospitalRepository extends JpaRepository<Hospital, Long> {
    Optional<Hospital> findOneByName(String name);
    // SEARCH, NAME OR CREATEDDATE
    Page<Hospital> findAllByNameContainingIgnoreCase(String name, Pageable pageable);
    // Page<Hospital> findAllByCreatedDateBetween(Instant data1, Instant data2, Pageable pageable);
    // select d.* from hospital d WHERE d.created_date >= '2021-02-01' and d.created_date < '2021-03-01'
    // Page<Hospital> findAllByNameContainingIgnoreCaseAndCreatedDate(String name, Instant createDate, Pageable pageable);

    // String query = "select d.* from hospital d WHERE d.name = :name1";

    // @Query( value = query, nativeQuery = true)
    // Page<Hospital> findAllByNameNative(
    //     @Param("name1") String date1,
    //     Pageable pageable
    // );

    String queryOne = "select * from hospital WHERE created_date >= :date1 and created_date < :date2";

    @Query( value = queryOne, nativeQuery = true)
    Page<Hospital> findAllByCreatedDateNative(
        @Param("date1") String date1,
        @Param("date2") String date2,
        Pageable pageable
    );

    String queryTwo = "select * from hospital WHERE created_date >= :date1 and created_date < :date2 and name = :name1";

    @Query( value = queryTwo, nativeQuery = true)
    Page<Hospital> findAllByNameAndCreatedDateNative(
        @Param("date1") String date1,
        @Param("date2") String date2,
        @Param("name1") String name,
        Pageable pageable
    );


}
