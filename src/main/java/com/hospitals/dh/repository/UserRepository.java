package com.hospitals.dh.repository;

import com.hospitals.dh.domain.User;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.time.Instant;

/**
 * Spring Data JPA repository for the {@link User} entity.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    String USERS_BY_LOGIN_CACHE = "usersByLogin";

    String USERS_BY_EMAIL_CACHE = "usersByEmail";

    Optional<User> findOneByActivationKey(String activationKey);

    List<User> findAllByActivatedIsFalseAndActivationKeyIsNotNullAndCreatedDateBefore(Instant dateTime);

    Optional<User> findOneByResetKey(String resetKey);

    Optional<User> findOneByEmailIgnoreCase(String email);

    Optional<User> findOneByLogin(String login);

    @EntityGraph(attributePaths = "authorities")
    @Cacheable(cacheNames = USERS_BY_LOGIN_CACHE)
    Optional<User> findOneWithAuthoritiesByLogin(String login);

    @EntityGraph(attributePaths = "authorities")
    @Cacheable(cacheNames = USERS_BY_EMAIL_CACHE)
    Optional<User> findOneWithAuthoritiesByEmailIgnoreCase(String email);

    Page<User> findAllByLoginNot(Pageable pageable, String login);

    Optional<User> findOneById(Long id);

    // QUERIES  ==========================================
    // DOCTORS
    String queryOne = "select distinct ju.* from (jhi_user ju JOIN extended_user eu on ju.id = eu.user_id), jhi_user_authority jua, jhi_authority ja where eu.hospital_doctor_id = :hospitalId " +
    "and jua.user_id like ju.id and jua.authority_name like ja.name and ja.name like 'ROLE_DOCTOR'";

    @Query( value = queryOne, nativeQuery = true)
    Page<User> findAllUsersByRoleAndHospitalId(
        @Param("hospitalId") String hospitalId,
        Pageable pageable
    );
    
    String queryTwo = "select distinct * " +
    "from jhi_user, jhi_user_authority jua, jhi_authority ja " +
    "where jua.user_id like id " +
    "and jua.authority_name like ja.name " +
    "and ja.name like :role";

    @Query( value = queryTwo, nativeQuery = true)
    Page<User> findAllUsersByRole(
        @Param("role") String role,
        Pageable pageable
    );
    // PATIENTS

    String queryThree = "select distinct ju.* from (jhi_user ju JOIN extended_user eu on ju.id = eu.user_id), jhi_user_authority jua, jhi_authority ja where eu.hospital_patient_id = :hospitalId " +
    "and jua.user_id like ju.id and jua.authority_name like ja.name and ja.name like 'ROLE_PATIENT'";

    @Query( value = queryOne, nativeQuery = true)
    Page<User> findAllPatientsByRoleAndHospitalId(
        @Param("hospitalId") String hospitalId,
        Pageable pageable
    );

    // SEARCHS DOCTORS =============================================================================
    String queryFour = "select distinct ju.* " +
    "from (jhi_user ju JOIN extended_user eu on ju.id = eu.user_id), jhi_user_authority jua, jhi_authority ja " +
    "where jua.user_id like ju.id " +
    "and jua.authority_name like ja.name " +
    "and ja.name like 'ROLE_DOCTOR' " +
    "and (ju.first_name like :firstName or ju.last_name like :lastName)";

    @Query( value = queryFour, nativeQuery = true)
    Page<User> searchDoctoresByName(
        @Param("firstName") String firstName,
        @Param("lastName") String lastName,
        Pageable pageable
    );

    String queryFive = "select distinct ju.* " +
    "from (jhi_user ju JOIN extended_user eu on ju.id = eu.user_id), jhi_user_authority jua, jhi_authority ja " +
    "where jua.user_id like ju.id " +
    "and jua.authority_name like ja.name " +
    "and ja.name like 'ROLE_DOCTOR' " +
    "and eu.birth_date <= :birthDate and eu.birth_date >= :birthDate";

    @Query( value = queryFive, nativeQuery = true)
    Page<User> searchDoctoresByCreatedDate(
        @Param("birthDate") String birdthDate,
        Pageable pageable
    );

    String querySix = "select distinct ju.* " +
    "from (jhi_user ju JOIN extended_user eu on ju.id = eu.user_id), jhi_user_authority jua, jhi_authority ja " +
    "where jua.user_id like ju.id " +
    "and jua.authority_name like ja.name " +
    "and ja.name like 'ROLE_DOCTOR' " +
    "and ((ju.first_name like :firstName or ju.last_name like :lastName) and eu.birth_date like :birdthDate)";

    @Query( value = querySix, nativeQuery = true)
    Page<User> searchDoctoresByNameAndCreatedDate(
        @Param("firstName") String firstName,
        @Param("lastName") String lastName,
        @Param("birdthDate") String birdthDate,
        Pageable pageable
    );

    // SEARCHS PATIENTS =============================================================================
    String query1 = "select distinct ju.* " +
    "from (jhi_user ju JOIN extended_user eu on ju.id = eu.user_id), jhi_user_authority jua, jhi_authority ja " +
    "where jua.user_id like ju.id " +
    "and jua.authority_name like ja.name " +
    "and ja.name like 'ROLE_PATIENT' " +
    "and (ju.first_name like :firstName or ju.last_name like :lastName)";

    @Query( value = query1, nativeQuery = true)
    Page<User> searchPatientsByName(
        @Param("firstName") String firstName,
        @Param("lastName") String lastName,
        Pageable pageable
    );

    String query2 = "select distinct ju.* " +
    "from (jhi_user ju JOIN extended_user eu on ju.id = eu.user_id), jhi_user_authority jua, jhi_authority ja " +
    "where jua.user_id like ju.id " +
    "and jua.authority_name like ja.name " +
    "and ja.name like 'ROLE_PATIENT' " +
    "and DATE(eu.birth_date) = :birthDate";

    @Query( value = query2, nativeQuery = true)
    Page<User> searchPatientsByCreatedDate(
        @Param("birthDate") String birdthDate,
        Pageable pageable
    );

    String query3 = "select distinct ju.* " +
    "from (jhi_user ju JOIN extended_user eu on ju.id = eu.user_id), jhi_user_authority jua, jhi_authority ja " +
    "where jua.user_id like ju.id " +
    "and jua.authority_name like ja.name " +
    "and ja.name like 'ROLE_PATIENT' " +
    "and (ju.first_name like :firstName or ju.last_name like :lastName) and DATE(eu.birth_date) = :birdthDate";

    @Query( value = query3, nativeQuery = true)
    Page<User> searchPatientsByNameAndCreatedDate(
        @Param("firstName") String firstName,
        @Param("lastName") String lastName,
        @Param("birdthDate") String birdthDate,
        Pageable pageable
    );    

    // select distinct ju.* from (jhi_user ju JOIN extended_user eu on ju.id = eu.user_id AND (eu.birth_date <= '2021-02-09' AND eu.birth_date >= '2021-02-09')), jhi_user_authority jua, jhi_authority ja where jua.user_id like ju.id and jua.authority_name like ja.name and ja.name like 'ROLE_DOCTOR'

}
