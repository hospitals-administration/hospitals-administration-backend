package com.hospitals.dh.repository;

import java.util.Optional;

import com.hospitals.dh.domain.Speciality;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Speciality entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SpecialityRepository extends JpaRepository<Speciality, Long> {
    Optional<Speciality> findOneByName(String name);
}
