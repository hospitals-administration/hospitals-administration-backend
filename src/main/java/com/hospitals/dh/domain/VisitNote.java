package com.hospitals.dh.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A VisitNote.
 */
@Entity
@Table(name = "visit_note")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class VisitNote extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    
    @Lob
    @Column(name = "description", nullable = false)
    private String description;

    @NotNull
    @Column(name = "visit_date", nullable = false)
    private LocalDate visitDate;

    @ManyToOne
    @JsonIgnoreProperties(value = "visitNotes", allowSetters = true)
    private User userPatient;

    @ManyToOne
    @JsonIgnoreProperties(value = "visitNotes", allowSetters = true)
    private User userDoctor;

    @ManyToOne
    @JsonIgnoreProperties(value = "visitNotes", allowSetters = true)
    private Hospital hospital;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public VisitNote description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getVisitDate() {
        return visitDate;
    }

    public VisitNote visitDate(LocalDate visitDate) {
        this.visitDate = visitDate;
        return this;
    }

    public void setVisitDate(LocalDate visitDate) {
        this.visitDate = visitDate;
    }

    public User getUserPatient() {
        return userPatient;
    }

    public VisitNote userPatient(User user) {
        this.userPatient = user;
        return this;
    }

    public void setUserPatient(User user) {
        this.userPatient = user;
    }

    public User getUserDoctor() {
        return userDoctor;
    }

    public VisitNote userDoctor(User user) {
        this.userDoctor = user;
        return this;
    }

    public void setUserDoctor(User user) {
        this.userDoctor = user;
    }

    public Hospital getHospital() {
        return hospital;
    }

    public VisitNote hospital(Hospital hospital) {
        this.hospital = hospital;
        return this;
    }

    public void setHospital(Hospital hospital) {
        this.hospital = hospital;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof VisitNote)) {
            return false;
        }
        return id != null && id.equals(((VisitNote) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "VisitNote{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            ", visitDate='" + getVisitDate() + "'" +
            "}";
    }
}
