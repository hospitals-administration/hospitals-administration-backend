package com.hospitals.dh.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A ExtendedUser.
 */
@Entity
@Table(name = "extended_user")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ExtendedUser extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "address")
    private String address;

    @Column(name = "birth_date")
    private LocalDate birthDate;

    @Column(name = "profile_photo")
    private String profilePhoto;

    @OneToOne
    @JoinColumn(unique = true)
    private User user;

    @ManyToOne
    @JsonIgnoreProperties(value = "extendedUsers", allowSetters = true)
    private Hospital hospitalDoctor;

    @ManyToOne
    @JsonIgnoreProperties(value = "extendedUsers", allowSetters = true)
    private Hospital hospitalPatient;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JoinTable(name = "extended_user_speciality",
               joinColumns = @JoinColumn(name = "extended_user_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "speciality_id", referencedColumnName = "id"))
    private Set<Speciality> specialities = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public ExtendedUser address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public ExtendedUser birthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getProfilePhoto() {
        return profilePhoto;
    }

    public ExtendedUser profilePhoto(String profilePhoto) {
        this.profilePhoto = profilePhoto;
        return this;
    }

    public void setProfilePhoto(String profilePhoto) {
        this.profilePhoto = profilePhoto;
    }

    public User getUser() {
        return user;
    }

    public ExtendedUser user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Hospital getHospitalDoctor() {
        return hospitalDoctor;
    }

    public ExtendedUser hospitalDoctor(Hospital hospital) {
        this.hospitalDoctor = hospital;
        return this;
    }

    public void setHospitalDoctor(Hospital hospital) {
        this.hospitalDoctor = hospital;
    }

    public Hospital getHospitalPatient() {
        return hospitalPatient;
    }

    public ExtendedUser hospitalPatient(Hospital hospital) {
        this.hospitalPatient = hospital;
        return this;
    }

    public void setHospitalPatient(Hospital hospital) {
        this.hospitalPatient = hospital;
    }

    public Set<Speciality> getSpecialities() {
        return specialities;
    }

    public ExtendedUser specialities(Set<Speciality> specialities) {
        this.specialities = specialities;
        return this;
    }

    public ExtendedUser addSpeciality(Speciality speciality) {
        this.specialities.add(speciality);
        speciality.getExtendUsers().add(this);
        return this;
    }

    public ExtendedUser removeSpeciality(Speciality speciality) {
        this.specialities.remove(speciality);
        speciality.getExtendUsers().remove(this);
        return this;
    }

    public void setSpecialities(Set<Speciality> specialities) {
        this.specialities = specialities;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ExtendedUser)) {
            return false;
        }
        return id != null && id.equals(((ExtendedUser) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ExtendedUser{" +
            "id=" + getId() +
            ", address='" + getAddress() + "'" +
            ", birthDate='" + getBirthDate() + "'" +
            ", profilePhoto='" + getProfilePhoto() + "'" +
            "}";
    }
}
