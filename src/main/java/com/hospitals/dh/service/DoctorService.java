package com.hospitals.dh.service;

import com.hospitals.dh.domain.Authority;
import com.hospitals.dh.domain.ExtendedUser;
import com.hospitals.dh.domain.User;
import com.hospitals.dh.repository.AuthorityRepository;
import com.hospitals.dh.repository.ExtendedUserRepository;
import com.hospitals.dh.repository.UserRepository;
import com.hospitals.dh.security.AuthoritiesConstants;
import com.hospitals.dh.security.SecurityUtils;
import com.hospitals.dh.service.dto.ExtendedUserDTO;
import com.hospitals.dh.service.dto.UserDTO;
import com.hospitals.dh.service.mapper.ExtendedUserMapper;
import com.hospitals.dh.service.mapper.HospitalMapper;
import com.hospitals.dh.service.mapper.UserMapper;
import com.hospitals.dh.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.security.RandomUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.CacheManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Service class for managing users.
 */
@Service
@Transactional
public class DoctorService {

    private final Logger log = LoggerFactory.getLogger(DoctorService.class);

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final AuthorityRepository authorityRepository;

    private final CacheManager cacheManager;

    private final ExtendedUserRepository extendedUserRepository;

    private final ExtendedUserMapper extendedUserMapper;

    private final HospitalMapper hospitalMapper;

    private final ExtendedUserService extendedUserService;

    private final UserMapper userMapper;

    public DoctorService(UserRepository userRepository, PasswordEncoder passwordEncoder, AuthorityRepository authorityRepository, CacheManager cacheManager, ExtendedUserRepository extendedUserRepository, ExtendedUserMapper extendedUserMapper, HospitalMapper hospitalMapper, ExtendedUserService extendedUserService, UserMapper userMapper) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.authorityRepository = authorityRepository;
        this.cacheManager = cacheManager;
        this.extendedUserRepository = extendedUserRepository;
        this.extendedUserMapper = extendedUserMapper;
        this.hospitalMapper = hospitalMapper;
        this.extendedUserService = extendedUserService;
        this.userMapper = userMapper;
    }

    @Transactional(readOnly = true)
    public Page<UserDTO> findAllUsersByDoctorRole(Pageable pageable) {
        log.debug("Request to get all user with doctor role");
        Page<UserDTO> users = userRepository.findAllUsersByRole("ROLE_DOCTOR", pageable).map(UserDTO::new);
        users.forEach((UserDTO userDTO) -> {
            this.getUserExtended(userDTO);
        });
        return users;
    }

    @Transactional(readOnly = true)
    public Page<UserDTO> findAllUsersByDoctorRoleAndHospitalId(String hospitalId, Pageable pageable) {
        log.debug("Request to get all user with doctor role and HospitalId");
        Page<UserDTO> users = userRepository.findAllUsersByRoleAndHospitalId(hospitalId, pageable).map(UserDTO::new);
        users.forEach((UserDTO userDTO) -> {
            this.getUserExtended(userDTO);
        });
        return users;
    }

    public void getUserExtended(UserDTO userDTO) {
        Optional<ExtendedUser> ue = extendedUserRepository.findById(userDTO.getId());
        ExtendedUser extendedUser = ue.get();
        ExtendedUserDTO extendedUserDTO = extendedUserMapper.toDto(extendedUser);
        if (ue != null) {
            userDTO.setAddress(extendedUserDTO.getAddress());
            userDTO.setBirthDate(extendedUserDTO.getBirthDate());
            userDTO.setProfilePhoto(extendedUserDTO.getProfilePhoto());
            userDTO.setHospitalDoctorId(extendedUserDTO.getHospitalDoctorId());
            userDTO.setHospitalPatientId(extendedUserDTO.getHospitalPatientId());
            userDTO.setSpecialities(extendedUserDTO.getSpecialities());
            userDTO.setHospitalDTO(this.hospitalMapper.toDto(extendedUser.getHospitalDoctor()));
        }
    }

    public User CreateDoctor(UserDTO userDTO, String password) {
        User user = new User();
        user.setLogin(userDTO.getLogin().toLowerCase());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        if (userDTO.getLogin() != null) {
            user.setEmail(userDTO.getLogin() + "@localhost".toLowerCase());
        }
        user.setImageUrl(userDTO.getImageUrl());
        if (userDTO.getLangKey() == null) {
            user.setLangKey("en"); // default language
        } else {
            user.setLangKey(userDTO.getLangKey());
        }
        String encryptedPassword = passwordEncoder.encode(userDTO.getLogin());
        user.setPassword(encryptedPassword);
        user.setResetKey(RandomUtil.generateResetKey());
        user.setResetDate(Instant.now());
        user.setActivated(true);
        Set<Authority> authorities = new HashSet<>();
        authorityRepository.findById(AuthoritiesConstants.DOCTOR).ifPresent(authorities::add);
        user.setAuthorities(authorities);

        User response = userRepository.save(user);
        if (response != null) {
            ExtendedUserDTO extendedUserDTO = new ExtendedUserDTO();
            extendedUserDTO.setId(response.getId());
            extendedUserDTO.setUserId(response.getId());
            extendedUserDTO.setAddress(userDTO.getAddress());
            extendedUserDTO.setBirthDate(userDTO.getBirthDate());
            extendedUserDTO.setHospitalDoctorId(userDTO.getHospitalDoctorId());
            extendedUserDTO.setSpecialities(userDTO.getSpecialities());
            this.extendedUserService.save(extendedUserDTO);
        }
        this.clearUserCaches(user);
        // User response = userRepository.save(newUser);
        log.debug("Created Information for User: {}", user);
        return user;
    }

    public Optional<UserDTO> updateDoctor(UserDTO userDTO) {
        return Optional.of(userRepository
            .findById(userDTO.getId()))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .map(user -> {
                this. clearUserCaches(user);
                user.setLogin(userDTO.getLogin().toLowerCase());
                user.setFirstName(userDTO.getFirstName());
                user.setLastName(userDTO.getLastName());
                if (userDTO.getEmail() != null) {
                    user.setEmail(userDTO.getEmail().toLowerCase());
                }
                user.setImageUrl(userDTO.getImageUrl());
                user.setActivated(true);
                user.setLangKey(userDTO.getLangKey());
                Set<Authority> managedAuthorities = user.getAuthorities();
                managedAuthorities.clear();
                userDTO.getAuthorities().stream()
                    .map(authorityRepository::findById)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .forEach(managedAuthorities::add);
                if (user != null) {
                    ExtendedUserDTO extendedUserDTO = new ExtendedUserDTO();
                    extendedUserDTO.setId(user.getId());
                    extendedUserDTO.setUserId(user.getId());
                    extendedUserDTO.setAddress(userDTO.getAddress());
                    extendedUserDTO.setBirthDate(userDTO.getBirthDate());
                    extendedUserDTO.setHospitalDoctorId(userDTO.getHospitalDoctorId());
                    extendedUserDTO.setSpecialities(userDTO.getSpecialities());
                    this.extendedUserService.save(extendedUserDTO);
                }
                this.clearUserCaches(user);
                log.debug("Changed Information for User: {}", user);
                return user;
            })
            .map(UserDTO::new);
    }

    public void deleteDoctor(Long id) {
        userRepository.findOneById(id).ifPresent(user -> {
            userRepository.delete(user);
            this.clearUserCaches(user);
            extendedUserRepository.deleteById(id);
            log.debug("Deleted User: {}", user);
        });
    }

    private void clearUserCaches(User user) {
        Objects.requireNonNull(cacheManager.getCache(UserRepository.USERS_BY_LOGIN_CACHE)).evict(user.getLogin());
        if (user.getEmail() != null) {
            Objects.requireNonNull(cacheManager.getCache(UserRepository.USERS_BY_EMAIL_CACHE)).evict(user.getEmail());
        }
    }

    public UserDTO addAvatar(Long id, MultipartFile file) throws IllegalStateException, IOException {
        String home = System.getProperty("user.home");
        String dirLocation = home + "/.images/user/avatar/";
        String dir = dirLocation;
        String typeImage = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1, file.getOriginalFilename().length());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String fileName = "";
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        if (typeImage.equals("jpg")) {
            fileName = "id_" + id.toString() + "_" + sdf.format(new Date()) + timestamp.getTime() + ".jpg";
        } else if (typeImage.equals("png")) {
            fileName = "id_" + id.toString() + "_" + sdf.format(new Date()) + timestamp.getTime() + ".png";
        } else if (typeImage.equals("jpeg")) {
            fileName = "id_" + id.toString() + "_" + sdf.format(new Date()) + timestamp.getTime() + ".jpeg";
        } else {
            // throw new BadRequestAlertException("Invalid Format", "userManagement", "invalid");
        }
        File dirLoc = new File(dir);
        if (!dirLoc.exists()) {
            dirLoc.mkdirs();
        }
        File secondFile = new File(dirLoc.getAbsolutePath() + "/" + fileName);
        file.transferTo(secondFile);
        
        User user = this.userRepository.findById(id).orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + id.toString()));
        String imageUrl = "/users" +"/get-image/" + fileName + "?url=" + "/user/avatar/";
        user.setImageUrl(imageUrl);

        user = this.userRepository.save(user);

        UserDTO userDTO2 = userMapper.userToUserDTO(user);
        return userDTO2;
    }

    @Transactional(readOnly = true)
    public Page<UserDTO> searchDoctorsByNameOrCreatedDate(String name, String birthdate, Pageable pageable) {
        log.debug("Request to get all user with doctor role");
        String query = "%" + name + "%";
        Page<UserDTO> users = userRepository.findAllUsersByRole("ROLE_DOCTOR",pageable).map(UserDTO::new);
        if (name != null) {
            users = userRepository.searchDoctoresByName(query, query, pageable).map(UserDTO::new);
        }
        if (birthdate != null) {
            users = userRepository.searchDoctoresByCreatedDate(birthdate, pageable).map(UserDTO::new);
        }
        if (name != null && birthdate != null) {
            users =userRepository.searchDoctoresByNameAndCreatedDate(query, query, birthdate, pageable).map(UserDTO::new);
        }
        users.forEach((UserDTO userDTO) -> {
            this.getUserExtended(userDTO);
        });
        return users;
    }


}