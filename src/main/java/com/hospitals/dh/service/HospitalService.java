package com.hospitals.dh.service;

import com.hospitals.dh.domain.Hospital;
import com.hospitals.dh.repository.HospitalRepository;
import com.hospitals.dh.service.dto.HospitalDTO;
import com.hospitals.dh.service.mapper.HospitalMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Hospital}.
 */
@Service
@Transactional
public class HospitalService {

    private final Logger log = LoggerFactory.getLogger(HospitalService.class);

    private final HospitalRepository hospitalRepository;

    private final HospitalMapper hospitalMapper;

    public HospitalService(HospitalRepository hospitalRepository, HospitalMapper hospitalMapper) {
        this.hospitalRepository = hospitalRepository;
        this.hospitalMapper = hospitalMapper;
    }

    /**
     * Save a hospital.
     *
     * @param hospitalDTO the entity to save.
     * @return the persisted entity.
     */
    public HospitalDTO save(HospitalDTO hospitalDTO) {
        log.debug("Request to save Hospital : {}", hospitalDTO);
        Hospital hospital = hospitalMapper.toEntity(hospitalDTO);
        hospital = hospitalRepository.save(hospital);
        return hospitalMapper.toDto(hospital);
    }

    /**
     * Get all the hospitals.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<HospitalDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Hospitals");
        return hospitalRepository.findAll(pageable)
            .map(hospitalMapper::toDto);
    }


    /**
     * Get one hospital by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<HospitalDTO> findOne(Long id) {
        log.debug("Request to get Hospital : {}", id);
        return hospitalRepository.findById(id)
            .map(hospitalMapper::toDto);
    }

    /**
     * Delete the hospital by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Hospital : {}", id);
        hospitalRepository.deleteById(id);
    }

    // ==========================
    @Transactional(readOnly = true)
    public Page<HospitalDTO> searchByNameOrCreatedDate(String name, String date, Pageable pageable) {
        log.debug("Search all Hospitals by name or created date");
        String query = "%" + name + "%";
        Page<HospitalDTO> hospitals = hospitalRepository.findAll(pageable).map(hospitalMapper::toDto);
        if (name != null) {
            hospitals = hospitalRepository.findAllByNameContainingIgnoreCase(name, pageable).map(hospitalMapper::toDto);
        }
        if (date != null) {
            final LocalDate myDate = LocalDate.parse(date);
            LocalDate data2 = myDate.plusDays(1); 
            hospitals = hospitalRepository.findAllByCreatedDateNative(myDate.toString(), data2.toString(), pageable).map(hospitalMapper::toDto);
        }
        if (name != null && date != null) {
            final LocalDate myDate = LocalDate.parse(date);
            LocalDate data2 = myDate.plusDays(1); 
            hospitals = hospitalRepository.findAllByNameAndCreatedDateNative(myDate.toString(), data2.toString(), name, pageable).map(hospitalMapper::toDto);
        }
        return hospitals;
    }


}
