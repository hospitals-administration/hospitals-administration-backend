package com.hospitals.dh.service;

import com.hospitals.dh.domain.VisitNote;
import com.hospitals.dh.repository.VisitNoteRepository;
import com.hospitals.dh.service.dto.VisitNoteDTO;
import com.hospitals.dh.service.mapper.VisitNoteMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link VisitNote}.
 */
@Service
@Transactional
public class VisitNoteService {

    private final Logger log = LoggerFactory.getLogger(VisitNoteService.class);

    private final VisitNoteRepository visitNoteRepository;

    private final VisitNoteMapper visitNoteMapper;

    public VisitNoteService(VisitNoteRepository visitNoteRepository, VisitNoteMapper visitNoteMapper) {
        this.visitNoteRepository = visitNoteRepository;
        this.visitNoteMapper = visitNoteMapper;
    }

    /**
     * Save a visitNote.
     *
     * @param visitNoteDTO the entity to save.
     * @return the persisted entity.
     */
    public VisitNoteDTO save(VisitNoteDTO visitNoteDTO) {
        log.debug("Request to save VisitNote : {}", visitNoteDTO);
        VisitNote visitNote = visitNoteMapper.toEntity(visitNoteDTO);
        visitNote = visitNoteRepository.save(visitNote);
        return visitNoteMapper.toDto(visitNote);
    }

    /**
     * Get all the visitNotes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<VisitNoteDTO> findAll(Pageable pageable) {
        log.debug("Request to get all VisitNotes");
        return visitNoteRepository.findAll(pageable)
            .map(visitNoteMapper::toDto);
    }


    /**
     * Get one visitNote by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<VisitNoteDTO> findOne(Long id) {
        log.debug("Request to get VisitNote : {}", id);
        return visitNoteRepository.findById(id)
            .map(visitNoteMapper::toDto);
    }

    /**
     * Delete the visitNote by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete VisitNote : {}", id);
        visitNoteRepository.deleteById(id);
    }

    // VIVIST BY PATIENT

    @Transactional(readOnly = true)
    public Page<VisitNoteDTO> findAllVisitNoteByPatientId(Long id, Pageable pageable) {
        log.debug("Request to get VisitNote : {}", id);
        return visitNoteRepository.findAllByUserPatientId(id, pageable)
            .map(visitNoteMapper::toDto);
    }

}
