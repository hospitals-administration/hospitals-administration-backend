package com.hospitals.dh.service.dto;

import java.time.LocalDate;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A DTO for the {@link com.hospitals.dh.domain.ExtendedUser} entity.
 */
public class ExtendedUserDTO extends AbstractAuditingDTO implements Serializable {
    
    private Long id;

    private String address;

    private LocalDate birthDate;

    private String profilePhoto;


    private Long userId;

    private Long hospitalDoctorId;

    private Long hospitalPatientId;
    private Set<SpecialityDTO> specialities = new HashSet<>();
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getProfilePhoto() {
        return profilePhoto;
    }

    public void setProfilePhoto(String profilePhoto) {
        this.profilePhoto = profilePhoto;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getHospitalDoctorId() {
        return hospitalDoctorId;
    }

    public void setHospitalDoctorId(Long hospitalId) {
        this.hospitalDoctorId = hospitalId;
    }

    public Long getHospitalPatientId() {
        return hospitalPatientId;
    }

    public void setHospitalPatientId(Long hospitalId) {
        this.hospitalPatientId = hospitalId;
    }

    public Set<SpecialityDTO> getSpecialities() {
        return specialities;
    }

    public void setSpecialities(Set<SpecialityDTO> specialities) {
        this.specialities = specialities;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ExtendedUserDTO)) {
            return false;
        }

        return id != null && id.equals(((ExtendedUserDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ExtendedUserDTO{" +
            "id=" + getId() +
            ", address='" + getAddress() + "'" +
            ", birthDate='" + getBirthDate() + "'" +
            ", profilePhoto='" + getProfilePhoto() + "'" +
            ", userId=" + getUserId() +
            ", hospitalDoctorId=" + getHospitalDoctorId() +
            ", hospitalPatientId=" + getHospitalPatientId() +
            ", specialities='" + getSpecialities() + "'" +
            "}";
    }
}
