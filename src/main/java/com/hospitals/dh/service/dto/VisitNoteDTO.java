package com.hospitals.dh.service.dto;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import javax.persistence.Lob;

/**
 * A DTO for the {@link com.hospitals.dh.domain.VisitNote} entity.
 */
public class VisitNoteDTO extends AbstractAuditingDTO implements Serializable {
    
    private Long id;

    
    @Lob
    private String description;

    @NotNull
    private LocalDate visitDate;


    private Long userPatientId;

    private Long userDoctorId;

    private Long hospitalId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(LocalDate visitDate) {
        this.visitDate = visitDate;
    }

    public Long getUserPatientId() {
        return userPatientId;
    }

    public void setUserPatientId(Long userId) {
        this.userPatientId = userId;
    }

    public Long getUserDoctorId() {
        return userDoctorId;
    }

    public void setUserDoctorId(Long userId) {
        this.userDoctorId = userId;
    }

    public Long getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(Long hospitalId) {
        this.hospitalId = hospitalId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof VisitNoteDTO)) {
            return false;
        }

        return id != null && id.equals(((VisitNoteDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "VisitNoteDTO{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            ", visitDate='" + getVisitDate() + "'" +
            ", userPatientId=" + getUserPatientId() +
            ", userDoctorId=" + getUserDoctorId() +
            ", hospitalId=" + getHospitalId() +
            "}";
    }
}
