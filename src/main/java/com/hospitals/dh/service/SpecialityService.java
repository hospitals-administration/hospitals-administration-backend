package com.hospitals.dh.service;

import com.hospitals.dh.domain.Speciality;
import com.hospitals.dh.repository.SpecialityRepository;
import com.hospitals.dh.service.dto.SpecialityDTO;
import com.hospitals.dh.service.mapper.SpecialityMapper;
import com.hospitals.dh.web.rest.errors.BadRequestAlertException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Speciality}.
 */
@Service
@Transactional
public class SpecialityService {

    private final Logger log = LoggerFactory.getLogger(SpecialityService.class);

    private final SpecialityRepository specialityRepository;

    private final SpecialityMapper specialityMapper;

    public SpecialityService(SpecialityRepository specialityRepository, SpecialityMapper specialityMapper) {
        this.specialityRepository = specialityRepository;
        this.specialityMapper = specialityMapper;
    }

    /**
     * Save a speciality.
     *
     * @param specialityDTO the entity to save.
     * @return the persisted entity.
     */
    public SpecialityDTO save(SpecialityDTO specialityDTO) {
        log.debug("Request to save Speciality : {}", specialityDTO);
        Speciality speciality = specialityMapper.toEntity(specialityDTO);
        speciality = specialityRepository.save(speciality);
        return specialityMapper.toDto(speciality);
    }

    /**
     * Get all the specialities.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<SpecialityDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Specialities");
        return specialityRepository.findAll(pageable)
            .map(specialityMapper::toDto);
    }


    /**
     * Get one speciality by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<SpecialityDTO> findOne(Long id) {
        log.debug("Request to get Speciality : {}", id);
        return specialityRepository.findById(id)
            .map(specialityMapper::toDto);
    }

    /**
     * Delete the speciality by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Speciality : {}", id);
        specialityRepository.deleteById(id);
    }

    public SpecialityDTO addAvatar(Long id, MultipartFile file) throws IllegalStateException, IOException {
        String home = System.getProperty("user.home");
        String dirLocation = home + "/.images/specialities/avatar/";
        String dir = dirLocation;
        String typeImage = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1, file.getOriginalFilename().length());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String fileName = "";
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        if (typeImage.equals("jpg")) {
            fileName = "id_" + id.toString() + "_" + sdf.format(new Date()) + timestamp.getTime() + ".jpg";
        } else if (typeImage.equals("png")) {
            fileName = "id_" + id.toString() + "_" + sdf.format(new Date()) + timestamp.getTime() + ".png";
        } else if (typeImage.equals("jpeg")) {
            fileName = "id_" + id.toString() + "_" + sdf.format(new Date()) + timestamp.getTime() + ".jpeg";
        } else {
            // throw new BadRequestAlertException("Invalid Format", "userManagement", "invalid");
        }
        File dirLoc = new File(dir);
        if (!dirLoc.exists()) {
            dirLoc.mkdirs();
        }
        File secondFile = new File(dirLoc.getAbsolutePath() + "/" + fileName);
        file.transferTo(secondFile);
        
        Speciality speciality = this.specialityRepository.findById(id).orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + id.toString()));
        String imageUrl = "/specialities" +"/get-image/" + fileName + "?url=" + "/specialities/avatar/";
        speciality.setAvatar(imageUrl);
        speciality = this.specialityRepository.save(speciality);
        SpecialityDTO userDTO2 = specialityMapper.toDto(speciality);

        return userDTO2;
    }

}
