package com.hospitals.dh.service.mapper;


import com.hospitals.dh.domain.*;
import com.hospitals.dh.service.dto.SpecialityDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Speciality} and its DTO {@link SpecialityDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SpecialityMapper extends EntityMapper<SpecialityDTO, Speciality> {


    @Mapping(target = "extendUsers", ignore = true)
    @Mapping(target = "removeExtendUser", ignore = true)
    Speciality toEntity(SpecialityDTO specialityDTO);

    default Speciality fromId(Long id) {
        if (id == null) {
            return null;
        }
        Speciality speciality = new Speciality();
        speciality.setId(id);
        return speciality;
    }
}
