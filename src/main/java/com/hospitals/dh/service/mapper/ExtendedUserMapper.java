package com.hospitals.dh.service.mapper;


import com.hospitals.dh.domain.*;
import com.hospitals.dh.service.dto.ExtendedUserDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ExtendedUser} and its DTO {@link ExtendedUserDTO}.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, HospitalMapper.class, SpecialityMapper.class})
public interface ExtendedUserMapper extends EntityMapper<ExtendedUserDTO, ExtendedUser> {

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "hospitalDoctor.id", target = "hospitalDoctorId")
    @Mapping(source = "hospitalPatient.id", target = "hospitalPatientId")
    ExtendedUserDTO toDto(ExtendedUser extendedUser);

    @Mapping(source = "userId", target = "user")
    @Mapping(source = "hospitalDoctorId", target = "hospitalDoctor")
    @Mapping(source = "hospitalPatientId", target = "hospitalPatient")
    @Mapping(target = "removeSpeciality", ignore = true)
    ExtendedUser toEntity(ExtendedUserDTO extendedUserDTO);

    default ExtendedUser fromId(Long id) {
        if (id == null) {
            return null;
        }
        ExtendedUser extendedUser = new ExtendedUser();
        extendedUser.setId(id);
        return extendedUser;
    }
}
