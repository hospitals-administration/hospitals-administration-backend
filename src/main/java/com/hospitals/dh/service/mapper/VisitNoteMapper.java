package com.hospitals.dh.service.mapper;


import com.hospitals.dh.domain.*;
import com.hospitals.dh.service.dto.VisitNoteDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link VisitNote} and its DTO {@link VisitNoteDTO}.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, HospitalMapper.class})
public interface VisitNoteMapper extends EntityMapper<VisitNoteDTO, VisitNote> {

    @Mapping(source = "userPatient.id", target = "userPatientId")
    @Mapping(source = "userDoctor.id", target = "userDoctorId")
    @Mapping(source = "hospital.id", target = "hospitalId")
    VisitNoteDTO toDto(VisitNote visitNote);

    @Mapping(source = "userPatientId", target = "userPatient")
    @Mapping(source = "userDoctorId", target = "userDoctor")
    @Mapping(source = "hospitalId", target = "hospital")
    VisitNote toEntity(VisitNoteDTO visitNoteDTO);

    default VisitNote fromId(Long id) {
        if (id == null) {
            return null;
        }
        VisitNote visitNote = new VisitNote();
        visitNote.setId(id);
        return visitNote;
    }
}
